

const gulp            	= require( 'gulp' ),
			gulpSass         	= require( 'gulp-sass' ),
			nodeSass         	= require( 'node-sass' ),
			sass         	   	= gulpSass( nodeSass ),
			sassGlob          = require( 'gulp-sass-glob' ),
			autoprefixer  		= require( 'autoprefixer' ),
			sortMediaQueries  = require( 'postcss-sort-media-queries' ),
			sourcemaps  			= require( 'gulp-sourcemaps' ),
			postcss 		  		= require( 'gulp-postcss' ),
			cssnano 		  		= require( 'cssnano' ),
			combineQueries  	= require( 'postcss-combine-media-query' ),
			combineSelectors	= require( 'postcss-combine-duplicated-selectors' ),
			browserSync   		= require( 'browser-sync' ),
			plumber       		= require( 'gulp-plumber' ),
			babel         		= require( 'gulp-babel' ),
			pug           		= require( 'gulp-pug' ),
			rename        		= require( 'gulp-rename' ),
			concat        		= require( 'gulp-concat' ),
			uglifyJs      		= require( 'gulp-uglify-es' ).default,
			optimizeImage 		= require( 'gulp-imagemin' ),
			svgSprite 		  	= require( 'gulp-svg-sprite' ),
			zip           		= require( 'gulp-zip' )


/* ------------------------------------------------------------------------- *\
**  === PATHS                                                                *|
\* ------------------------------------------------------------------------- */
let base = './dist';

let path = {
	server: {
		src: './src/',
		dest: base
	},
	html: {
		src: './src/pug/*.pug',
		dest: base,
		watch: './src/pug/**/*.pug'
	},
	sass: {
		src: ['./src/sass/white-space.sass'],
		dest: base + '/assets/css',
		watch: ['./src/sass/**/*.sass', './src/sass/**/*.css'],
	},
	js: {
		src: './src/js/main.js',
		dest: base + '/assets/js'
	},
	jsplugins: {
		src: './src/js/plugins/*.js',
		dest: base + '/assets/js'
	},
	img: {
		src: './src/img/*.*',
		dest: base + '/assets/img'
	},
	svg: {
		src: './src/img/svg/*.svg',
		dest: base + '/assets/img'
	},
	font: {
		src: './src/fonts/*',
		dest: base + '/assets/fonts'
	}
}


/* ------------------------------------------------------------------------- *\
**  === BROWSER SYNC                                                         *|
\* ------------------------------------------------------------------------- */
function browser_init(open = false) {
	browserSync.init({
		port: 3000,
		open: open,
		ui: false,
		notify: false
	});
}


/* ------------------------------------------------------------------------- *\
**  === TASKS                                                                *|
\* ------------------------------------------------------------------------- */
function html_pug(){
	return gulp.src(path.html.src)
		.pipe( plumber() )
		.pipe( pug({ pretty: '\t' }) )
		.on( 'error', function (err) { console.log(err) } )
		.pipe( gulp.dest(path.html.dest) )
		.pipe( browserSync.stream() );
}

function style_sass(){
	return gulp.src(path.sass.src)
		.pipe( plumber() )
		.pipe( sourcemaps.init() )
    .pipe( sassGlob() )
    .pipe( sass().on('error', sass.logError) )
		.pipe( sourcemaps.write('.') )
		.pipe( gulp.dest(path.sass.dest))
		.pipe( browserSync.stream({match: '**/*.css'}) )
}

// function js_script(){
// 	return gulp.src(path.js.src)
// 		.pipe( plumber() )
// 		.pipe( babel({ "presets": ["es2015"] }) )
// 		.pipe( gulp.dest(path.js.dest) )
// 		.pipe( browserSync.stream() )
// }

// function js_plugin(){
// 	return gulp.src(path.jsplugins.src)
// 		.pipe( concat( 'plugins.js' ) )
// 		.pipe( uglifyJs() )
// 		.pipe( rename({ suffix: '.min' }) )
// 		.pipe( gulp.dest(path.jsplugins.dest) )
// 		.pipe( browserSync.stream() )
// }

function img_optimization(){
	return gulp.src(path.img.src)
		.pipe( optimizeImage() )
		.pipe( gulp.dest(path.img.dest) )
		.pipe( browserSync.stream() )
}

function svg_sprite(){
	return gulp.src(path.svg.src)
		.pipe(svgSprite({
			// shape: {
			// 	dimension: {
			// 		maxWidth: 200,
			// 		maxHeight: 200
			// 	}
			// },
			mode 	: {
				symbol: {
					render: {
						css: true,
						scss: true,
					},
					dest: '.',
					sprite: 'sprite.svg',
					example: true
				}
			}
		}))
		.pipe(gulp.dest(path.svg.dest) )
		.pipe( browserSync.stream() )
}

function font_copy() {
	return gulp.src(path.font.src)
		.pipe( gulp.dest(path.font.dest) )
		.pipe( browserSync.stream() )
}


/* ------------------------------------------------------------------------- *\
**  === BUILD                                                                *|
\* ------------------------------------------------------------------------- */
function min_css() {
	var plugins = [
		cssnano(),
		autoprefixer(),
		combineSelectors(),
		sortMediaQueries(),
		combineQueries()
	];

	return gulp.src(path.sass.dest + "/white-space.css")
		.pipe( postcss(plugins) )
		.pipe( rename({suffix: '.min'}) )
		.pipe( gulp.dest(path.sass.dest));
}
// function min_js() {
// 	return gulp.src(path.js.dest + "/main.js")
// 		.pipe( plumber() )
// 		.pipe( uglifyJs() )
// 		.pipe( rename({ suffix: '.min' }) )
// 		.pipe( gulp.dest(path.js.dest) )
// }
function zip_files() {
	return gulp.src(path.server.dest)
		.pipe( zip('release.zip') )
		.pipe( gulp.dest('.') )
}


/* ------------------------------------------------------------------------- *\
**  === GULP EXPORTS                                                         *|
\* ------------------------------------------------------------------------- */
function watch_files() {
	browser_init()

	gulp.watch(path.html.watch, html_pug)
	gulp.watch(path.sass.watch, gulp.series(style_sass, min_css))
  // gulp.watch(path.js.src, gulp.series(js_script, min_js))
	// gulp.watch(path.jsplugins.src, js_plugin)
	gulp.watch(path.img.src, img_optimization)
	gulp.watch(path.svg.src, svg_sprite)
	gulp.watch(path.font.src, font_copy)
}

const gulp_pug    = gulp.series(html_pug)
const gulp_sass   = gulp.series(style_sass)
// const gulp_js     = gulp.series(js_script)
// const gulp_plugin = gulp.series(js_plugin)
const gulp_img    = gulp.series(img_optimization)
const gulp_svg    = gulp.series(svg_sprite)
const gulp_font   = gulp.series(font_copy)

const gulp_dist  = gulp.parallel(gulp_pug, gulp_sass, gulp_img, gulp_svg, gulp_font)
const gulp_min   = gulp.parallel(min_css)
const gulp_zip   = gulp.series(zip_files)
const gulp_build = gulp.series(gulp_min, gulp_zip)
const gulp_watch = gulp.parallel(gulp_dist, watch_files)

module.exports = {
  pug    : gulp_pug,
  sass   : gulp_sass,
  // js     : gulp_js,
  // plugin : gulp_plugin,
  img    : gulp_img,
  svg    : gulp_svg,
  dist   : gulp_dist,
	watch  : gulp_watch,
	build  : gulp_build,
  default: gulp_watch
}