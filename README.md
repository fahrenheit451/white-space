# White Space

[![version](https://flat.badgen.net/badge/release/v0.0.2/blue)](https://gitlab.com/fahrenheit451/white-space/-/tree/main)
[![license](https://flat.badgen.net/badge/license/ISC/orange)](https://fr.wikipedia.org/wiki/Licence_ISC)
[![author1](https://flat.badgen.net/badge/author/Leandre%20Nargeot/green)](https://gitlab.com/fahrenheit451)

---

<div align="center"><strong>Spacing, simple spacing</strong></div>

## Description
White Space is a sass spacer based on the height

## Quick start
Add white space in your ```<head>```
```html
<link rel="stylesheet" type="text/css" href="css/white-space.min.css"/>
```
And... that's it

## How to use
In the html you can add an ```ws-30``` for 30px spacing

|                         | Extra small       | Small             | Medium            | Large             | Extra large       | Extra Extra large |
| :---------------------- | :---------------- | :---------------- | :---------------- | :---------------- | :---------------- | :---------------- |
| Breakpoints             | <576px            | ≥576px            | ≥768px            | ≥992px            | ≥1200px           | ≥1400px           |
| Class prefix (positive) | ```.ws-```        | ```.ws-sm-```     | ```.ws-md-```     | ```.ws-lg-```     | ```.ws-xl-```     | ```.ws-xxl-```    |
| Class prefix (negative) | ```.ws-n-```      | ```.ws-n-sm-```   | ```.ws-n-md-```   | ```.ws-n-lg-```   | ```.ws-n-xl-```   | ```.ws-n-xxl-```  |



## Changelog
* v0.0.2
  * update sass loop
  * update README.md
* v0.0.1
  * hello ! 


### License
Copyright (C) 2019

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.